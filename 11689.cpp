#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;

int main()
{
    int n, e, f, c, sum, soda=0, temp;
    scanf("%d", &n);
    for(int j=1; j<=n; ++j){
        scanf("%d%d%d", &e, &f, &c);
        sum=e+f;
        while(sum>=c){
            temp=sum/c;
            soda += temp;
            sum = sum - (temp*c);
            sum+=temp;
        }
        printf("%d\n", soda);
        soda=0, temp=0, sum=0;
    }


    return 0;
}